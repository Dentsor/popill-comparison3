package net.dentsor.popill.comparison;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Core {
    public static void main(String[] args) {

        Printer.println(Arrays.asList(args));
        Printer.space();

        if (args.length < 2 || args.length > 2) {
            Printer.println("**** ***, I NEED 2 ENTRIES!");
        } else {
            File file1 = new File(args[0]);
            File file2 = new File(args[1]);

            if (file1.isDirectory() && file2.isDirectory()) {
                String[] files1 = file1.list();
                String[] files2 = file2.list();

                String list1 = "", list2 = "", listN = "";

                for (String entry : files1) {
                    boolean match = false;
                    for (String hit : files2) {
                        if (entry.equalsIgnoreCase(hit)) {
                            match = true;
                        }
                    }
                    if (match) {
                        listN += entry + System.getProperty("line.separator");
                    } else {
                        list1 += entry + System.getProperty("line.separator");
                    }
                }

                for (String entry : files2) {
                    boolean match = false;
                    for (String hit : files1) {
                        if (entry.equalsIgnoreCase(hit)) {
                            match = true;
                        }
                    }
                    if (!match) {
                        list2 += entry + System.getProperty("line.separator");
                    }
                }

                Printer.println("Directory 1:");
                Printer.space();
                Printer.println(list1);
                Printer.space(3);
                Printer.println("Directory 2:");
                Printer.space();
                Printer.println(list2);
                Printer.space(3);
                Printer.println("Neutral (in both):");
                Printer.space();
                Printer.println(listN);
            } else if (file1.isFile() && file2.isFile()) {

                try {
                    String stringA = readFile(file1.getPath());
                    String stringB = readFile(file2.getPath());

                    String aLines[] = stringA.split("\r?\n|\r");// "\\r?\\n");
                    String bLines[] = stringB.split("\r?\n|\r");// "\\r?\\n");

                    java.util.List<String> aList = new ArrayList<String>();
                    java.util.List<String> bList = new ArrayList<String>();
                    java.util.List<String> nList = new ArrayList<String>(); // neutral

                    for (String dEntry : aLines) {
                        boolean match = false;
                        for (String iEntry : bLines) {
                            if (dEntry.equalsIgnoreCase(iEntry)) {
                                match = true;
                            }
                        }
                        if (match) {
                            nList.add(dEntry);
                        } else {
                            aList.add(dEntry);
                        }
                    }


                    for (String iEntry : bLines) {
                        if (!nList.contains(iEntry)) {
                            boolean match = false;
                            for (String dEntry : aLines) {
                                if (iEntry.equalsIgnoreCase(dEntry)) {
                                    match = true;
                                }
                            }
                            if (match) {
                                nList.add(iEntry);
                            } else {
                                bList.add(iEntry);
                            }
                        }
                    }

                    String d = null, i = null, n = null, ls = System.getProperty("line.separator");

                    for (String e : aList) {
                        if (d == null)
                            d = e;
                        else
                            d += (ls + e);
                    }
                    for (String e : nList) {
                        if (n == null)
                            n = e;
                        else
                            n += (ls + e);
                    }
                    for (String e : bList) {
                        if (i == null)
                            i = e;
                        else
                            i += (ls + e);
                    }

                    Printer.space(2);
                    Printer.println("File 1:");
                    Printer.space();
                    Printer.println(d);
                    Printer.space(3);
                    Printer.println("File 2:");
                    Printer.space();
                    Printer.println(i);
                    Printer.space(3);
                    Printer.println("Neutral (in both)");
                    Printer.space();
                    Printer.println(n);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                Printer.println("Not valid files / directories");
            }
        }
    }

    public static String readFile(String fileName) throws IOException {
        if (new File(fileName).exists()) {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                return sb.toString();
            } finally {
                br.close();
            }
        } else {
            throw new FileNotFoundException(fileName);
        }
    }
}
